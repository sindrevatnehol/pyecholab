.. currentmodule:: echolab.integration

Integration
===========
At present, echolab only supports integration of mean Sv from single-beam data:

Single-beam data
----------------

.. autofunction:: integrate_single_beam_Sv
