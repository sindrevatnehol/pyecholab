Installing pyecholab
====================


Required Packages
-----------------

pyecholab requries the following additional python packages:

  * `pandas <http://pandas.pydata.org>`_ [>= 0.11]
    
    pyecholab has been built around version 0.11 of the pandas package.  As of now, 0.11 is the *development* version of pandas, available from the git repository
    `<https://github.com/pydata/pandas>`_

  * `numpy <http://www.numpy.org>`_ [>= 1.7.0]

    Numerical python library.

  * `h5py <http://code.google.com/p/h5py>`_ [>= 2.0.0]

    HDF5 python library.  Used to export/import data.

  * `pytz <http://pytz.sourceforge.net>`_


Recommended Packages
--------------------

  * `matplotlib <http://matplotlib.sourceforge.net>`_  

    Used for :mod:`echolab.plotting`
  
  * `ipython <http://ipython.org>`_ 

    Much nicer interactive python shell.


Installing
----------

Run setup.py to install pyecholab

.. code-block:: bash

  $python setup.py install  



