Integrating Sv
==============

Integrating data with echolab involves a number of steps, none of which are too complicated but care should be used to do things in the correct order:

  #. Load some data into python
  #. Apply calibration settings stored in Echoview's ECS format
  #. Calculate linear sv values to be used for integration
  #. Create grids and masks
  #. Integrate the linear sv data
  #. Examine the results



Using echolab interactively works best with the ipython shell with matplotlib's pylab loaded inline:

.. code-block:: bash

  $ ipython --pylab
  Python 2.7.2 (default, Jun 12 2011, 15:08:59) [MSC v.1500 32 bit (Intel)]
  Type "copyright", "credits" or "license" for more information.

  IPython 0.13.1 -- An enhanced Interactive Python.
  ?         -> Introduction and overview of IPython's features.
  %quickref -> Quick reference.
  help      -> Python's own help system.
  object?   -> Details about 'object', use 'object??' for extra details.

  Welcome to pylab, a matplotlib-based Python environment [backend: TkAgg].
  For more information, type 'help(pylab)'.

First import the echolab package.  We will also use the logging package to give some insight on what echolab is doing in the background:

.. ipython::
  :verbatim:

  In [1]: import echolab

  In [2]: import logging

  In [3]: logging.basicConfig(level=logging.DEBUG)    


Load some data:

.. ipython::
  :verbatim:

  In [6]: raw_filename = r'src\test\data\test_buddy_resize2-D20110712-T132550.raw'

  In [7]: bot_filename = r'src\test\data\test_buddy_resize2-D20110712-T132550.bot'

  In [10]: data = echolab.RawReader([raw_filename, bot_filename])
  INFO:echolab._io.raw_reader:Reading raw file 1:  src\test\data\test_buddy_resize2-D20110712-T132550.raw
  DEBUG:echolab._io.raw_reader:    Parsed 10000 datagrams (4920 sample, 5080 NMEA, 0 tag, 0 bottom).
  INFO:echolab._io.raw_reader:  Read 11408 datagrams (0 skipped, 5610 sample, 5798 NMEA, 0 tag, 0 bottom).
  INFO:echolab._io.raw_reader:Data spans time range of 2011-07-12 13:25:50.428381+00:00 to 2011-07-12 13:51:07.447853+00:00
  INFO:echolab._io.raw_reader:Reading bottom file 1:  src\test\data\test_buddy_resize2-D20110712-T132550.bot
  INFO:echolab._io.raw_reader:  Read 1122 datagrams (0 skipped, 0 sample, 0 NMEA, 0 tag, 1122 bottom).
  INFO:echolab._io.raw_reader:Read 12530 datagrams total (5610 sample, 5798 NMEA, 0 tag, 1122 bottom)
  INFO:echolab._io.raw_reader:Creating data structures...
  DEBUG:echolab._io.raw_reader:  Creating sample data DataFrame
  DEBUG:echolab._io.raw_reader:  Creating nmea DataFrame
  DEBUG:echolab._io.raw_reader:  Creating bottom data DataFrame


Load the calibration file:

.. ipython::
  :verbatim:

  In [6]: ecs_filename = r'src\test\data\test_calibration.ecs'  

  In [7]: data.load_calibration(ecs_filename)

  In [8]: data.calibration_params
  {1: {'absorption_coefficient': 0.009978,
    'angle_offset_alongship': 0.08,
    'angle_offset_athwartship': 0.01,
    'beamwidth_alongship': 6.93,
    'beamwidth_athwartship': 6.93,
    'equivalent_beam_angle': -20.69,
    'gain': 25.15,
    'sa_correction': -0.64,
    'sound_velocity': 1470.0},
   2: {'absorption_coefficient': 0.02906,
    'equivalent_beam_angle': -21.09,
    'gain': 27.95,
    'sa_correction': -0.42,
    'sound_velocity': 1470.0}}  

Now we have a :class:`echolab.io.RawReader` object with our data but we need to define our grid and mask settings.  For this example we will use bottom-referenced data with a bottom-referenced grid.

  * Integration grid:

    -  Interval size of 0.5 nmi
    -  Layer thickness of 10m
    -  Layer reference:  bottom

  * Integration masks:

    -  Exclude everything from three meters above the bottom and below
    -  Exclude everything from 10 meters depth and above (example of using a depth, not a range)
    -  Exclude samples w/ log Sv < -70dB
    -  Exclude samples w/ log Sv > -30dB


Before we can create our mask and grid we need to align some information to correspond to the timestamps of the data.  Our grid uses distance for the interval size, so we need to estimate vessel distance at a per-ping resolution.  We also intended to use bottom ranges as a reference, so we will interpolate the reported bottom range for each ping as well (removing no-bottom records and missing records along the way.)

.. ipython::
  :verbatim:

  #Interpolates bottom *range* for each ping
  In [16]: data.interpolate_ping_bottom_range()
  DEBUG:echolab.io:Checking for sound velocity calibration data

  #Interpolate vessel distance
  In [17]: data.interpolate_ping_dist(ignore_checksum=True)
  INFO:echolab.io:Interpolating VLW fixes...
  INFO:echolab.io:  Joining...

  #Interpolate gps coordinates, just for fun.
  In [18]: data.interpolate_ping_gps(ignore_checksum=True)
  INFO:echolab.io:No GPS datagrams of type GLL
  INFO:echolab.io:Found 1518 datagrams for NMEA type GGA
  INFO:echolab.io:  Parsing and converting lat/lon to decimal degrees...
  INFO:echolab.io:  Interpolating data to ping timestamps...
  DEBUG:echolab.io:    Backfilling NaN values...  


Now we can calculate linear sv in preparation for the integration step.  This is a two-step process.  
  
  #. linear sv is calculated from the data.
  #. data is arranged according to the desired reference


.. ipython::
  :verbatim:

  #Calculate linear sv
  In [15]: data.Sv(linear=True)

  #Create a 2D array with linear_sv for channel 1 arranged according our
  #desired reference, the bottom.
  In [20]: linear_sv = data.to_array('sv', channel=1, reference='bottom')
  DEBUG:echolab.io:  Found lat ping data...
  DEBUG:echolab.io:  Found lon ping data...
  DEBUG:echolab.io:  Found distance ping data...
  DEBUG:echolab.io:  Found bottom ping data...

Objects returned by :ref:`echolab.io.RawReader.to_array` behave like :ref:`Masked Arrays <numpy:maskedarray>` with an additional attribute to track a few extra bits of information.  See :ref:`axesarray` for specifics to echolab.


With our new referenced data object we can construct our grid.  Again, this is a two-step process:

  #. Define a grid parameter object with our grid specifications
  #. Create a grid object based on actual data.


.. ipython::
  :verbatim:

  #Define the grid parameter object
  In [8]: grid_params = echolab.grid.GridParameters(layer_spacing=10, 
     ...:   layer_unit='range', interval_spacing=0.5, interval_unit='distance')

  #Use the parameter object to construct a grid around our data
  In [26]: grid = grid_params.grid(linear_sv)  

The final step before actual integration:  defining masks.  We work with masks like we do with grids:  by first defining mask parameters and then constructing a mask based on actual data.  This allows a single definition of grids and masks to be used across multiple datasets.

The integration function expects masks to follow a certain naming convention to follow with Echoview's analysis results.  "Exclude above" and "Exclude below" type masking require either the corresponding submasks to be named 'exclude_above' and 'exclude_below', or the names of these submasks to be provided to the function arguments `exclude_above_submask_name` and `exclude_below_submask_name`.

.. ipython::
  :verbatim:

  In [11]: mask = echolab.mask.EchogramMask()

  #Bottom exclusion mask
  #Constant range reference of 3m off bottom.  Since our data is bottom-referenced,
  #the bottom has been placed at range 0, so -3m is "3m above bottom"
  #We name it 'exclude_below' so that the integration function knows how to calculate
  #the 'mean_exclude_bottom_depth_line' parameter.
  In [13]: mask.mask_below_range(name='exclude_below', reference=-3)

  #Surface reference line
  #The surface exclusion mask is a bit trickier.  Since the bottom is the new range=0,
  #the original transducer face sits at range -bottom, as stored in the original data object.
  #Furthermore, this bottom value is *range*, not *depth*.  To get the true water surface
  #with respect to our shifted bottom value:
  In [13]: surface = -data.get_channel(1)['bottom'] - data.get_channel(1)['transducer_depth']

  #Surface exclusion mask
  #now we can construct an exclusion for all samples above 10m *depth*
  #We name it 'exclude_above' for the same reason we named the previous
  #filter 'exclude_below'.
  In [14]: mask.mask_above_range(name='exclude_above', reference=surface + 10)


Notice we haven't defined the sample threshold masks in our mask object.  We could, the mask object has the methods to do it, but threshold masks are treated separately to ease comparison with integration products produced by Echolab.

.. warning:: 

  Do NOT define min/max sample threshold masks in the :ref:`echolab.mask.EchogramMask` object with the intention to use it for integration.  Instead, define your min/max threshold values as parameters to the integration function itself.  The return integration result objects will include entries for these threshold masks.


Finally, we can integrate.  Note that the min/max thresholds have been defined in the integration function call, and we can define these thresholds in the log domain for ease of use.

.. ipython::
  :verbatim:

  In [14]: int_results, mask_results = echolab.integration.integrate_single_beam_Sv(data=linear_sv,
     ....:    grid=grid,  mask=mask, min_threshold=-70, max_threshold=-30, log_thresholds=True)
    DEBUG:echolab.integration:    [ 0.91%]... integrating (1, -20)
    DEBUG:echolab.integration:    [ 1.13%]... integrating (1, -19)
    DEBUG:echolab.integration:    [ 1.36%]... integrating (1, -18)
    DEBUG:echolab.integration:    [ 1.59%]... integrating (1, -17)
    ...
    DEBUG:echolab.integration:    [98.19%]... integrating (9, 17)
    DEBUG:echolab.integration:    [98.41%]... integrating (9, 18)
    DEBUG:echolab.integration:    [98.64%]... integrating (9, 19)
    DEBUG:echolab.integration:    [98.87%]... integrating (9, 20)


So what are int_results and mask_results?
.. ipython::
  :verbatim:

  In [45]: int_results
  <class 'pandas.core.frame.DataFrame'>
  MultiIndex: 192 entries, (1, -20) to (9, 0)
  Data columns:
  Sv_max                  192  non-null values
  Sv_mean                 192  non-null values
  Sv_min                  192  non-null values
  abc                     192  non-null values
  depth_mean              192  non-null values
  end_ping                192  non-null values
  end_timestamp           192  non-null values
  excluded_samples        192  non-null values
  height_mean             192  non-null values
  integrated_samples      192  non-null values
  max_depth               192  non-null values
  max_range               192  non-null values
  min_depth               192  non-null values
  min_range               192  non-null values
  nasc                    192  non-null values
  num_filtered_pings      192  non-null values
  num_integrated_pings    192  non-null values
  num_valid_pings         192  non-null values
  range_mean              192  non-null values
  start_ping              192  non-null values
  start_timestamp         192  non-null values
  thickness_mean          192  non-null values
  valid_samples           192  non-null values
  dtypes: datetime64[ns](2), float64(13), int64(8)


  In [46]: mask_results
  <class 'pandas.core.frame.DataFrame'>
  MultiIndex: 192 entries, (1, -20) to (9, 0)
  Data columns:
  (sample, min_threshold)             192  non-null values
  (sample, exclude_below)             192  non-null values
  (sample, exclude_above)             192  non-null values
  (sample, max_threshold)             192  non-null values
  dtypes: int64(4)  


Integration results are pandas DataFrame objects, just as the underlying sample, bottom, nmea, and tag data.  This is where learning something about how the `pandas <http://pandas.pydata.org/pandas-docs/dev/index.html>`_ package works will take you places.

For echolab-specific help, see :ref:`integration_results_`
