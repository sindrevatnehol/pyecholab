﻿# coding=utf-8

# Copyright (c) 2012, Zac Berkowitz
#     National Oceanic and Atmospheric Administration (NOAA)
#     Alaskan Fisheries Science Center (AFSC)
#     Resource Assessment and Conservation Engineering (RACE)
#     Midwater Assessment and Conservation Engineering (MACE)

# All rights reserved.

# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:

# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.

# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation 
#     and/or other materials provided with the distribution.

# 3.  Neither the names of NOAA, AFSC, RACE, or MACE nor the names of its 
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.


# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

u'''
.. module:: echolab.util.gps


| Developed by:  Zac Berkowitz <zac.berkowitz@gmail.com> under contract for
| National Oceanic and Atmospheric Administration (NOAA)
| Alaska Fisheries Science Center (AFSC)
| Midwater Assesment and Conservation Engineering Group (MACE)
|
| Maintained by:
|       Zac Berkowitz <zac.berkowitz@gmail.com>
|       Rick Towler   <rick.towler@noaa.gov>

$Id$
'''

import numpy as np

def haversin(x):
    '''
    haversine(x) = sin(x/2)^2
    
    x is in radians
    '''
    
    return np.sin(x/2)**2

def archaversin(x):
    '''
    inverse haversine
    
    archarversine(x) = 2 * arcsin(sqrt(x))
    '''
    
    return 2 * np.arcsin(np.sqrt(x))


def gps_distance(lat0, lon0, lat1, lon1, r=6356.78):
    '''
    Calculates the distance between two gps coordinates using the haversine
    formula.  Uses the value 'r' for the radius of the Earth (in km).
    
    haversin(d/r) = haversin(lat1 - lat0) + cos(lat0)*cos(lat1)*haversin(lon1-lon0)
    
    so
    
    d = r * archaversin(RHS)
    
    :returns: distance (in meters)
    '''
    
    r_m = r * 1e3
    
    PI_DIV_180 = np.pi/180
    lat0_r = lat0 * PI_DIV_180
    lat1_r = lat1 * PI_DIV_180
    lon0_r = lon0 * PI_DIV_180
    lon1_r = lon1 * PI_DIV_180
    
    RHS = haversin(lat1_r - lat0_r) + np.cos(lat0_r)*np.cos(lat1_r)*haversin(lon1_r-lon0_r)
    
    d = r_m * archaversin(RHS)
    
    return d