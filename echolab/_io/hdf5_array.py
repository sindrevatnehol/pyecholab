﻿# coding=utf-8

# Copyright (c) 2012, Zac Berkowitz
#     National Oceanic and Atmospheric Administration (NOAA)
#     Alaskan Fisheries Science Center (AFSC)
#     Resource Assessment and Conservation Engineering (RACE)
#     Midwater Assessment and Conservation Engineering (MACE)

# All rights reserved.

# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:

# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.

# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation 
#     and/or other materials provided with the distribution.

# 3.  Neither the names of NOAA, AFSC, RACE, or MACE nor the names of its 
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.


# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

u'''
.. module:: echolab.export

    :synopsis:  Export data to various formats


    Provides functions for exporting data to file formats other than
    SIMRAD's raw format

        array_to_hdf5     -      Exports an AxesArray obj to hdf5
        array_from_hdf5   -      Imports an objs from hdf5

| Developed by:  Zac Berkowitz <zac.berkowitz@gmail.com> under contract for
| National Oceanic and Atmospheric Administration (NOAA)
| Alaska Fisheries Science Center (AFSC)
| Midwater Assesment and Conservation Engineering Group (MACE)
|
| Maintained by:
|       Zac Berkowitz <zac.berkowitz@gmail.com>
|       Rick Towler   <rick.towler@noaa.gov>

$Id$
'''

import h5py
import cPickle
import pandas as pd
import numpy as np
from datetime import timedelta
import zlib

from echolab.AxesArray import AxesArray
from echolab.util.date_conversion import UTC_UNIX_EPOCH


def convert_axes_with_datetime_to_float(axes):
    '''
    Converts record-array axes w/ 'reduced_timestamp'
    fields containing :class:`datetime.datetime` objects
    to # of seconds past UTC EPOCH.
    
    Required because h5py cannot store general objects
    in arrays.
    '''
    if axes is None:
        return axes
    
    fields = axes.dtype.fields.copy()

    if 'reduced_timestamp' not in fields:
        return axes

    #Need to make a new axes vector converting
    fields['reduced_timestamp'] = ('float64', None)
   
    new_dtype = [(k, v[0]) for k,v in fields.items()]
    
    new_axes = np.empty(axes.shape, dtype=new_dtype)
    
    for field in fields:
        if field == 'reduced_timestamp':
            continue
        
        new_axes[field][:] = axes[field][:]
        
    new_axes['reduced_timestamp'][:] = np.array([(x - UTC_UNIX_EPOCH).total_seconds() for x in axes['reduced_timestamp'][:]])
    
    return new_axes

def convert_axes_with_floats_to_datetime(axes):
    '''
    Converts record-array axes w/ 'reduced_timestamp'
    fields containing floats representing the
    # of seconds past UTC EPOCH to :class:`datetime.datetime` objects
    
    Required because h5py cannot store general objects
    in arrays.
    '''
    if axes is None:
        return axes
    
    fields = axes.dtype.fields.copy()

    if 'reduced_timestamp' not in fields:
        return axes
    
    #Need to make a new axes vector converting
    fields['reduced_timestamp'] = ('O', None)
   
    new_dtype = [(k, v[0]) for k,v in fields.items()]
    
    new_axes = np.empty(axes.shape, dtype=new_dtype)
    
    for field in fields:
        if field == 'reduced_timestamp':
            continue
        
        new_axes[field][:] = axes[field][:]
        
    new_axes['reduced_timestamp'][:] = np.array([timedelta(seconds=x) + UTC_UNIX_EPOCH for x in axes['reduced_timestamp']])
    
    return new_axes

def array_to_hdf5(filename, data, info=None, mask=None, grid=None,
        norm=None, integration_results=None, mask_results=None, 
        post_mask_func=None):
    '''
    :param filename:  Filename of saved file.
    :type filename: str
    
    :param data: Data to save
    :type data: :class:`echolab.AxesArray.AxesArray`
    
    :param info: Dictionary of additional info to save as attributes.
    :type info: dict
    
    :param mask:  Mask object
    :type mask: :class:`echolab.mask.EchogramMask`
    
    :param grid: Grid paremter object
    :type grid: :class:`echolab.grid.GridParameters`
    
    :param norm:  Normalization function
    
    :param post_mask_func:  Callable function to use on data AFTER masks
        are stored but BEFORE data is stored
    
    
    Data is stored as a regular hdf5 dataset.  Mask, grid, and norm,
    if provided, are pickled with cPickle for storage as string-arrays.
    
    `norm`
    Using a norm has the potential for massive file size savings
    at the expense of reduced data resolution (ek500_norm only has
    13 values from -70 to -34 dB).  If you're interested in saving
    data for display purposes only, using a norm is very helpful.
        
    For example, the :attr:`echolab.plotting.ek500_norm` is a
    :class:`matplotlib.colors.BoundaryNorm` object set to the
    typical Echoview colorscheme for Sv.
    
    `post_mask_func`
    A function applied to data after boolean masks are created from
    the mask object (if provided).
    
    A bit of a kludge to store log-Sv values after integrating/masking using
    linear sv.
    '''

    data_info = getattr(data, 'info', {})
    data_info.update({'version': 1, 'type': 'echolab'})
    data_axes = []
    data_axes.extend(getattr(data, 'axes', [None] * data.ndim))
    
    for n in range(len(data_axes)):
        data_axes[n] = convert_axes_with_datetime_to_float(data_axes[n])
    
    
    with h5py.File(filename, 'w') as h5file:
        #Need to run masks before norming..
        mask_group = h5file.create_group('masks')
    
        if mask is not None:
            for mask_name, submask in mask.submask.items():
                _ = mask_group.create_dataset(mask_name, data=submask.mask(data),
                        compression='gzip', compression_opts=9)

        if post_mask_func is not None:
            data = post_mask_func(data)

        if norm is not None:
            if not callable(norm):
                raise RuntimeError('Provided norm is not callable')
            
    
            

            _convert_to_int = False
            
            try:
                data = norm(data)
            
            except MemoryError:
                log.warning('Ran out of memory norming array of %.2f %s samples', np.prod(data.shape)/1.0e6, data.dtype)
                _convert_to_int = True
                
                
            if _convert_to_int:
                log.warning('Attempting to convert array to int16\'s w/ no norm')
                
                try:
                    data = data.astype('int16')
                except MemoryError:
                    log.warning('  Unable to convert data to int16\'s, saving full-blown data')
                  
                #Remove norm
                norm = None               
                
                    
            else:         
                #Matplotlib boundary norms have len(norm.boundaries) + 2 levels
                #  an index of -1 is for values below the minimum norm value
                #  an index of len(norm.boundaries) is for values above the max norm value
                #  and finally an index of 'fill_value' if for masked values
                
                num_levels = len(norm.boundaries) + 3
                
                if num_levels < 2**8 - 1:
                    dtype = 'uint8'
                    fill_value = 2**8 - 1
                
                elif num_levels < 2**16 - 1:
                    dtype = 'uint16'
                    fill_value = 2**16 - 1
                
                elif num_levels < 2**32 - 1:
                    dtype = 'uint32'
                    fill_value = 999999
                
                else:
                    dtype = 'uint64'
                    fill_value = 999999
                    
                
                #Shift data to 0 level.
                data -= data.min()    
                data = data.astype(dtype)
                data.fill_value = fill_value
                
            
            dtype = data.dtype
            fill_value = data.fill_value
        
        if info is not None:
            for attrib_name, attrib_value in info.items():
                h5file.attrs[attrib_name] = attrib_value if attrib_value is not None else 'NULL'
        
        #Store transceiver information as a separate group
        transceiver_group = h5file.create_group('transceiver')
        for attrib_name, attrib_value in data_info.get('transceiver_info', {}).items():
            transceiver_group.attrs[attrib_name] = attrib_value
            
        #Create dataset w/ data
        data_dset = h5file.create_dataset('data', data=data.filled(), 
                        compression='gzip', compression_opts=9)            
                
        #Store info besides transceiver as attributes on dataset
        for attrib_name, attrib_value in data_info.items():
            if attrib_name == 'transceiver_info':
                continue
            data_dset.attrs[attrib_name] = attrib_value
        
        #Add the fill value as an attribute
        data_dset.attrs['fill_value'] = data.fill_value

        #Create the sample axis dataset
        sample_axis = h5file.create_dataset('sample_axis', data=data_axes[0],
                  compression='gzip', compression_opts=9)
        
        #Same for ping axis dataset and units
        ping_axis = h5file.create_dataset('ping_axis', data=data_axes[1],
                    compression='gzip', compression_opts=9)
             
        pickle_group = h5file.create_group('pickled_objs')
        pandas_group = h5file.create_group('pandas_objs')
        
        #Supplied a GridParameters object?   Store that too
        if grid is not None:
            #Pickle & Compress object 
            compressed_data = zlib.compress(cPickle.dumps(grid, cPickle.HIGHEST_PROTOCOL), 9)
            #Create a void (opaque) hdf5 type w/ length= compressed data
            #i.e. create a type "|V500" for len(compressed_data) = 500
            dt = h5py.h5t.PYTHON_OBJECT.copy()
            dt.set_size(len(compressed_data))
            
            #Create a scalar dataset w/ this void data type
            ds = pickle_group.create_dataset('grid', shape=(), dtype=dt)
            
            #Insert the compressed string
            ds[()] = compressed_data
            
        
        #Mask object?
        if mask is not None:
            compressed_data = zlib.compress(cPickle.dumps(mask, cPickle.HIGHEST_PROTOCOL), 9)
            dt = h5py.h5t.PYTHON_OBJECT.copy()
            dt.set_size(len(compressed_data))
            ds = pickle_group.create_dataset('mask', shape=(), dtype=dt)
            ds[()] = compressed_data
       
        #Norm?
        if norm is not None:
            compressed_data = zlib.compress(cPickle.dumps(norm, cPickle.HIGHEST_PROTOCOL), 9)
            dt = h5py.h5t.PYTHON_OBJECT.copy()
            dt.set_size(len(compressed_data))
            ds = pickle_group.create_dataset('norm', shape=(), dtype=dt)
            ds[()] = compressed_data

    #Integration results?
    if integration_results is not None:
        integration_results.to_hdf(filename, '/pandas_objs/integration_results', table=False, append=False)


    #Mask results?            
    if mask_results is not None:
        mask_results.to_hdf(filename, '/pandas_objs/mask_results', table=False, append=False)
    
def array_from_hdf5(filename):
    '''
    :param filename:  Filename of saved file.
    :type filename: str
    
    :returns: dict
    
    
    Returns a dictionary with the following keys:
    
    'data':    :class:`echolab.AxesArray.AxesArray` object
    'info':    Dict of additional information
    'mask_obj'::class:`echolab.mask.EchogramMask` object or None
    'masks'    dict of boolean ndarrays of mask output.
    'grid':    :class:`echolab.grid.GridParameters` or None
    'norm':    Norm used to scale data, or None
    'integration_results'  Integration results or None
    'mask_results':        Mask results or None   
    '''
    
    with h5py.File(filename, 'r') as h5file:
        
        info = {}
        info.update(h5file.attrs)
        
        transceiver_info = {}
        transceiver_info.update(h5file['transceiver'].attrs)
        
        data_dset = h5file['data']
        
        data_info = {}
        data_info.update(data_dset.attrs)
        for k,v in data_info.items():
            if v == 'NULL':
                data_info[k] = None

        data_info['transceiver_info'] = transceiver_info
        
        fill_value = data_info.pop('fill_value')
        
        sample_axis = h5file['sample_axis'][:]
        
        ping_axis = convert_axes_with_floats_to_datetime(h5file['ping_axis'][:])      
       
        masks = {}
        for mask_name in h5file['masks']:
            masks[mask_name] = h5file['masks'][mask_name][:]
        
        if 'grid' in h5file['pickled_objs']:
            grid = cPickle.loads(zlib.decompress(h5file['pickled_objs']['grid'][()]))
        else:
            grid = None
            
        if 'mask' in h5file['pickled_objs']:
            mask_obj = cPickle.loads(zlib.decompress(h5file['pickled_objs']['mask'][()]))
        else:
            mask_obj = None
            
        if 'norm' in h5file['pickled_objs']:
            norm = cPickle.loads(zlib.decompress(h5file['pickled_objs']['norm'][()]))
        else:
            norm = None
            
        if 'integration_results' in h5file['pandas_objs']:
            integration_results = pd.read_hdf(filename, '/pandas_objs/integration_results')
            
        else:
            integration_results = None
    
        if 'mask_results' in h5file['pandas_objs']:
            mask_results = pd.read_hdf(filename, '/pandas_objs/mask_results')
        else:
            mask_results = None
        
        data = data_dset.value.astype('int')
        if norm is not None:
            #Normed data is 0-indexed:
            #  0:  value below norm threshold, replace with -inf
            #  len(norm.boundaries) + 1:  value above norm threshold, replace with +inf
            #  fill_value:  index should be masked
            
            low_mask = data == 0
            high_mask = data == len(norm.boundaries) + 1
            nan_mask = data == fill_value
            
            #Set highmask|nanmask = len(norm.boundaries)
            data[high_mask|nan_mask] = len(norm.boundaries)
            
            #Use norm.boundaries to rebuild values.  
            data = norm.boundaries[data-1].astype('float32')
            
            data[high_mask] = np.inf
            data[low_mask] = -np.inf

        else:
            nan_mask = data == fill_value
            
        #Rebuild axes array object
        data = AxesArray(input_array=data, 
                         axes=[sample_axis, ping_axis], 
                         copy=True,
                         info=data_info)
        
        data[nan_mask] = np.ma.masked
        data.fill_value = fill_value
                         
    return dict(data=data, info=info, mask_obj=mask_obj, masks=masks, 
                grid=grid, norm=norm, integration_results=integration_results,
                mask_results=mask_results)