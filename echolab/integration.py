﻿# coding=utf-8

# Copyright (c) 2012, Zac Berkowitz
#     National Oceanic and Atmospheric Administration (NOAA)
#     Alaskan Fisheries Science Center (AFSC)
#     Resource Assessment and Conservation Engineering (RACE)
#     Midwater Assessment and Conservation Engineering (MACE)

# All rights reserved.

# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:

# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.

# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.

# 3.  Neither the names of NOAA, AFSC, RACE, or MACE nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.


# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

u'''
.. :module:: echolab.integration
    :synopsis:  Functions for integrating acoustic data

| Developed by:  Zac Berkowitz <zac.berkowitz@gmail.com> under contract for
| National Oceanic and Atmospheric Administration (NOAA)
| Alaska Fisheries Science Center (AFSC)
| Midwater Assesment and Conservation Engineering Group (MACE)
|
| Maintained by:
|       Zac Berkowitz <zac.berkowitz@gmail.com>
|       Rick Towler   <rick.towler@noaa.gov>
'''

import pandas as pd
import logging

log = logging.getLogger(__name__)

def integrate_single_beam_Sv(data, grid, mask=None, min_threshold=None,
        max_threshold=None, max_threshold_replacement=None,
        adaptive_mean_thickness=False, log_thresholds=False,
        exclude_above_mask_name='exclude_above',
        exclude_below_mask_name='exclude_below'):
    '''
    :param data: Linear sv data
    :type data: :class:`echolab.AxesArray.AxesArray`

    :param grid: grid object
    :type grid: :class:`echolab.grid.Grid`

    :param mask: mask object, or None
    :type mask: :class:`echolab.mask.EchogramMask`

    :param min_threshold: Minimum threshold value
    :type min_threshold: float

    :param max_threshold: Maximum threshold
    :type max_threshold: float

    :param max_threshold_replacement: Value to replace samples > max_threshold
    :type max_threshold_replacement: None

    :param adaptive_mean_thickness: Completely masked pings reduce mean thickness?
    :type adaptive_mean_thickness: bool

    :param log_thresholds:  Provided min/max thresholds are log valued?
    :type log_thresholds: bool

    :param exclude_above_mask_name: Name of submask to use for 'exclude above'
    :type exclude_above_mask_name: str

    :param exclude_below_mask_name: Name of submask to use for 'exclude below'
    :type exclude_below_mask_name: str

    :returns: (integration_results, mask_results), both type :class:`pandas.core.DataFrame` [#df]_



    Calculates mean Sv values based on provided grid and linear sv values for
    single-beam data.  At present, no ability for creating 'classes' of
    for integration is available.

    A mask can be provided to exclude regions, pings, or individual samples.
    However, the mask should NOT perform min/max thresholding.  Again:

    .. warning::
        Do NOT define min/max threshold masks in `mask`, use the function
        parameters!

    This is to make it easier to compare echolab generated analysis against
    Echoview values.

    This function treats min/max thresholding like Echoview:
        *  If min_threshold is not None, all samples below the threshold will be
           excluded from the mean Sv estimate.

        *  If max_threshold is not None and

           -  max_threshold_replacement is None:

                all values above max_threshold will be excluded from the
                mean Sv estimate

           -  max_threshold_replacement is not None:

                all values above max_threshold will be set to max_threshold
                and included in the mean Sv estimate.

        This dual-treatment of max_threshold is the principal reason why including
        min/max sample masks through the mask parameter is not used.


    This function will return (integration_results, mask_results) of type
    :class:`pandas.core.DataFrame` [#df]_  Both use a hierarchical index of
    (interval_id, layer_id) tuples.

    * integration_results object columns:
        :Sv_max:                 Max Sv value in cell
        :Sv_mean:                Mean Sv value in cell
        :Sv_min:                 Min Sv value in cell
        :abc:                    Acoustic Backscattering Coefficient
        :nasc:                   Nautical Area Scattering Coefficient
        :depth_mean:             mean depth of cell
        :ping_end:               last ping included in cell
        :timestamp_end:          timestamp of last ping
        :valid_samples:          number of samples in cell with data
        :excluded_samples:       number of valid samples excluded from
                                 integration due to masking (exclude above,
                                 below, etc.) [#int_samples]_
        :integrated_samples:     number of samples included in integration
        :height_mean:            mean height of cell
        :depth_max:              maximum depth of cell
        :range_max:              maximum range of cell
        :num_filtered_pings:     number of whole-pings excluded by ping filters
        :num_integrated_pings:   number of pings included in integration
        :num_valid_pings:        number of pings w/ at least one included sample
        :range_mean:             mean range of cell
        :ping_start:             first included ping in cell
        :timestamp_start:        timestamp of first ping
        :thickness_mean:         mean thickness of cell
        :start_lat:              latitude of first ping in cell
        :end_lat:                latitude of last ping in cell
        :start_lon:              longitude of first ping in cell
        :end_lon:                longitude of last ping in cell

    *  mask_results object columns:
        -  mask_results columns are hierarchal tuples of (mask_type, mask_name),
           for example:

             -  ('sample', 'exclude_below_threshold')
             -  ('ping', 'ringdown')

        -  (row, column) pairs yield the number of elements masked a particular
           filter for a given (interval, layer).


    .. [#df] `<http://pandas.pydata.org/pandas-docs/dev/dsintro.html>`_
    .. [#int_samples]  This is the number of samples contributing to the
        "integration volume" term in mean-Sv.  For instance, an sample
        excluded because it's value is below the minimum threshold would still
        be counted as "integrated" since it contributes to the integration volume
        even though the value is replaced by 0.
    '''
    #Construct masks
    mask_results = {}
    full_mask = pd.np.zeros_like(data, dtype='bool')
    full_mask._mask = full_mask._mask.copy()
    full_mask.fill_value = True

    exclude_above_mask = None
    exclude_above_bool_mask = None

    exclude_below_mask = None
    exclude_below_bool_mask = None

    masks = {}
    mask_results = {}

    if mask is None:
        full_mask[:, :] = False

    elif isinstance(mask, pd.np.ndarray):
        if mask.shape != data.shape:
            raise IndexError('Supplied ndarray mask shape != data shape')

        masks['sample'] = dict(ndarray=mask.astype('bool'))
        full_mask[:, :] = masks['sample']['ndarray']

        mask_results['sample']['ndarray'] = []

    elif isinstance(mask, pd.np.ma.masked_array):
        if mask.shape != data.shape:
            raise IndexError('Supplied masked_array mask shape != data shape')

        sample_masks = dict(masked_array=mask.filled(1).astype('bool'))
        ping_masks = {}
        full_mask[:,:] = sample_masks['masked_array']

        masks['sample'] = dict(masked_array=mask.filled(1).astype('bool'))
        full_mask[:, :] = masks['sample']['masked_array']

        mask_results['sample']['masked_array'] = []

    elif hasattr(mask, 'submask'):
        for name, submask in mask.submask.items():

            if not submask.is_enabled:
                log.debug('    mask: %s is disabled, skipping...', name)
                continue

            mask_type = submask.mask_type.lower()

            if mask_type in ['sample', 'ping', 'composite']:

                mask_group = masks.setdefault(mask_type, {})
                mask_group[name] = submask.mask(data)
                full_mask[:, :] |= mask_group[name]

                mask_results[(mask_type, name)] = []

                if name == exclude_below_mask_name:
                    exclude_below_bool_mask = mask_group[name]
                    exclude_below_mask = submask

                elif name == exclude_above_mask_name:
                    exclude_above_bool_mask = mask_group[name]
                    exclude_above_mask = submask

            else:
                log.warning('Unknown mask type: %s', mask_type)
                continue

    else:
        raise TypeError('Unable to create masks from object of type %s'\
                        % (str(type(mask))))


    if data.info.get('data_type', None) != 'sv':
        raise ValueError('Expected linear sv values')

    if log_thresholds:
        if min_threshold is not None:
            min_threshold = 10 **(min_threshold / 10.0)

        if max_threshold is not None:
            max_threshold = 10 **(max_threshold / 10.0)

    #SINGLE BEAM integration has a constant sample volume of 1
#    sample_volume = 1

    #Threshold masks
    if min_threshold is not None:
        min_threshold_mask  = data <= min_threshold
        mask_results[('sample', 'exclude_below_threshold')] = []

    if max_threshold is not None:
        max_threshold_mask = data >= max_threshold
        mask_results[('sample', 'exclude_above_threshold')] = []

    if max_threshold_replacement is None:
        max_threshold_replacement = pd.np.ma.masked
    else:
        max_threshold_replacement = max_threshold


#  This geometry calculation code is incorrect - the angle_offset_* values are not
#  related to the physical orientation of the transducer and thus shouldn't be used
#  when calculating the mean height of an integration cell. The code has been
#  commented here, and references to height_projection_factor removed later in the
#  function. This code may be of use for integrating data from transducers that are
#  not mounted with their face parallel to the sea surface.
#
#  THIS FUNCTION ASSUMES THAT THE TRANSDUCER IS MOUNTED WITH THE FACE PARALLEL
#  TO THE SEA SURFACE.
#
#    #Geometry calculation
#    transceiver = data.info.get('transceiver_info', {})
#
#    #Geometry offsets converted to radians
#    alongship_offset = transceiver.get('angle_offset_alongship', 0.0) * pd.np.pi / 180.0
#    athwartship_offset = transceiver.get('angle_offset_athwartship', 0.0) * pd.np.pi / 180.0
#
#    if (alongship_offset == 0) and (athwartship_offset == 0):
#        height_projection_factor = 1.0
#    else:
#        #Alpha is the combined projection from the transducer pointing direction to vertical-down
#        alpha = pd.np.arctan(pd.np.sqrt(pd.np.tan(alongship_offset)**2 + pd.np.tan(athwartship_offset)**2))
#        height_projection_factor = pd.np.cos(alpha)


    results_index_tuples = []
    results = dict(Sv_mean=[],
                   Sv_min=[],
                   Sv_max=[],
                   integrated_samples=[],
                   valid_samples=[],
                   excluded_samples=[],
                   thickness_mean=[],
                   height_mean=[],
                   depth_mean=[],
                   depth_min=[],
                   depth_max=[],
                   range_mean=[],
                   range_min=[],
                   range_max=[],
                   abc=[],
                   nasc=[],
                   ping_start=[],
                   ping_end=[],
                   num_valid_pings=[],
                   num_integrated_pings=[],
                   num_filtered_pings=[],
                   timestamp_start=[],
                   timestamp_end=[],
                   mean_exclude_above=[],
                   mean_exclude_below=[],
                   lat_start=[],
                   lat_end=[],
                   lon_start=[],
                   lon_end=[])

    num_intervals = len(grid.get_interval_ids())
    num_layers = len(grid.get_layer_ids())

    #  iterate through the intervals
    for _i, interval_id in enumerate(grid.get_interval_ids()):

        #Calculate mean exclude above/below if such masks are present

        #mean exclude above/below is the mean value of the line used to
        #create the lower/upper bounds of the respective mask.

        start_ping, end_ping = grid.intervals_df.loc[interval_id][['start_ping', 'end_ping']]
        #End ping in intervals_df is INCLUSIVE
        end_ping += 1

        try:
            lat_start, lat_end = grid.intervals_df.loc[interval_id][['start_lat', 'end_lat']]
        except KeyError:
            lat_start, lat_end = pd.np.nan, pd.np.nan

        try:
            lon_start, lon_end = grid.intervals_df.loc[interval_id][['start_lon', 'end_lon']]
        except KeyError:
            lon_start, lon_end = pd.np.nan, pd.np.nan


        #Assumptions:  exlude_above and exclude_below return boolean masks
        #with values = True for regions above/below the exclusion line

        #To find the range value of this line, sum the number of samples in
        #each ping to find the offset from the array top (for exclude above) or
        #array bottom (for exclude below) of the line.  Then use that
        #index against the data's 'range' axis to reproduce the reference line
        #in range units.  Finally, take the mean of this recomputed reference
        #line
        if exclude_above_mask is not None:
            if pd.np.isscalar(exclude_above_mask.reference):
                mean_exclude_above = exclude_above_mask.reference
            else:
                mean_exclude_above = pd.np.mean(exclude_above_mask.reference[start_ping:end_ping])
        else:
            mean_exclude_above = -9999

        if exclude_below_mask is not None:
            if pd.np.isscalar(exclude_below_mask.reference):
                mean_exclude_below = exclude_below_mask.reference
            else:
                mean_exclude_below = pd.np.mean(exclude_below_mask.reference[start_ping:end_ping])
        else:
            mean_exclude_below = -9999


        #  iterate through the layers
        for _l, layer_id in enumerate(grid.get_layer_ids()):

            data_cell = grid.get_data_by_cell(data, interval=interval_id,
                            layer=layer_id)

            if data_cell is None:
                continue

            num_valid_samples = pd.np.sum(~data_cell.mask)

            cell_sample_exclusion_mask = grid.get_data_by_cell(full_mask, interval=interval_id,
                                                               layer=layer_id)

            num_excluded_samples =  pd.np.sum(cell_sample_exclusion_mask & (~data_cell.mask))

            data_cell[cell_sample_exclusion_mask] = pd.np.ma.masked

            #eps_S is a mask where
            #     eps_S = 0 if the sample is excluded from the analysis domain
            #           = 1 otherwise
            #
            #  A sample is considered "excluded" if
            #     it lies below an 'exclude-below' line
            #     it lies above an 'exclude-above' line
            #     it is considered a 'no-data' value
            #            (i.e. outside original recording range)
            #   see: http://support.echoview.com/WebHelp/Reference/Algorithms/About_analysis_domains.htm#Exclusions
            #
            #Our mask is 1 if excluded, 0 otherwise, so eps_S is the simple inversion
            eps_S = ~data_cell.mask
            sum_eps_S = eps_S.sum()

            #  Check if no valid samples were in this cell and if so, check if we're
            #  below the exclude below line in which case we are done with this interval
            if sum_eps_S == 0:
                #  all samples are masked for this cell
                log.debug('    [%5.2f%%]... (%d, %d) completely masked...',
                          100.0*(_i * num_layers + _l) / (num_intervals * num_layers),
                          interval_id, layer_id)

                #  check if we're done with this interval
                if exclude_below_bool_mask is not None:
                    _m = grid.get_data_by_cell(exclude_below_bool_mask, interval=interval_id,
                                               layer=layer_id)
                    if _m.all():
                        log.debug('    (%d, %d) completely masked by exclude below, skipping remaining layers...',
                                 interval_id, layer_id)
                        break
                continue

            log.debug('    [%5.2f%%]... (%d, %d) integrating...',
                      100.0*(_i * num_layers + _l) / (num_intervals * num_layers),
                      interval_id, layer_id)

            #  Apply maximum threshold
            #    data values >= threshold are set to max_threshold_replacement (by default this
            #    is pd.np.ma.masked) Thresholded samples are still considered valid (ie. eps_S = 1)
            if max_threshold is not None:
                cell_max_threshold_mask = grid.get_data_by_cell(max_threshold_mask, interval=interval_id,
                                                                layer=layer_id)
                data_cell[cell_max_threshold_mask & eps_S] = max_threshold_replacement
                samples_above_max_threshold = (cell_max_threshold_mask & eps_S).sum()

                if max_threshold_replacement is pd.np.ma.masked:
                    num_excluded_samples += samples_above_max_threshold

            #Harden mask to prevent overwriting masked values w/ 0's from min-threshold operation
            data_cell.harden_mask()

            #  Apply minimum threshold
            #    data values <= threshold are set to 0, but samples are still considered valid (ie. eps = 1)
            if min_threshold is not None:
                cell_min_threshold_mask = grid.get_data_by_cell(min_threshold_mask, interval=interval_id,
                                                                layer=layer_id)
                data_cell[cell_min_threshold_mask & eps_S] = 0
                samples_below_min_threshold = (cell_min_threshold_mask & eps_S).sum()
                num_excluded_samples += samples_below_min_threshold

            #  calculate the mean sv
            sv_mean = data_cell.sum() / sum_eps_S

            #  Check if sv_mean == np.ma.masked - This can happen if all the samples
            #  left after the mask exclusion step are then excluded due to value thresholding
            if (sv_mean is pd.np.ma.masked) or (sv_mean <= 0):
                sv_mean = 0
                log_sv_mean = 0
            else:
                log_sv_mean = 10 * pd.np.log10(sv_mean)

            #  get the minimum sv in this cell
            #  first check if there is any data left above the minimum threshold
            sv_min = data_cell.data[data_cell > 0]
            if (sv_min.shape[0] > 0):
                #  there is, get the minimum
                sv_min = sv_min.min()
            else:
                #  all data is below the min threshold
                sv_min = 0
            #  caclulate min Sv
            if (sv_min is pd.np.ma.masked) or (sv_min <= 0):
                log_sv_min = 0
            else:
                log_sv_min = 10 * pd.np.log10(sv_min)

            #  get the maximum sv in this cell
            sv_max = data_cell.max()
            if (sv_max is pd.np.ma.masked) or (sv_max <= 0):
                log_sv_max = 0
            else:
                log_sv_max = 10 * pd.np.log10(sv_max)

            #Thickness mean calculation
            #  Assumptions:  sample thickness is constant
            ping_thickness = data.info['meters_per_sample'] * eps_S.sum(axis=0)
            if adaptive_mean_thickness:
                N_p = pd.np.sum(ping_thickness > 0)
            else:
                N_p = float(len(ping_thickness))

            thickness_mean = ping_thickness.sum() / N_p

            #  as noted above, height_projection_factor shouldn't be used and has been removed
            #  from the code below. The old lines of code have been commented out

            #height_mean = thickness_mean * height_projection_factor
            height_mean = thickness_mean

            #  mean range of cell
            range_mean = (data_cell.axes[0]['range'] * eps_S.sum(axis=1)).sum()/sum_eps_S

            #  Depth mean is the range mean with the transducer draft
            transducer_draft_mean = pd.np.mean(data_cell.axes[1]['transducer_depth'])
            #depth_mean = range_mean * height_projection_factor + transducer_draft_mean
            depth_mean = range_mean + transducer_draft_mean

            #  get the min and max ranges
            range_min, range_max = data_cell.axes[0]['range'][eps_S.any(axis=1)][[0, -1]]

            #  calculate the min and max depths
            #depth_min, depth_max = [x * height_projection_factor + transducer_draft_mean
            #                            for x in [range_min, range_max]]
            depth_min, depth_max = [x + transducer_draft_mean for x in [range_min, range_max]]

            #Area Backscattering Coefficient
            abc = thickness_mean * sv_mean

            #Nautical Area Scattering Coefficient (4pi * 1852**2 * abc)
            nasc = 4.0 * pd.np.pi * 3429904.0 * abc

            #  insert results into output structure
            results_index_tuples.append((interval_id, layer_id))
            results['Sv_mean'].append(log_sv_mean)
            results['Sv_min'].append(log_sv_min)
            results['Sv_max'].append(log_sv_max)
            results['integrated_samples'].append(sum_eps_S)
            results['valid_samples'].append(num_valid_samples)
            results['excluded_samples'].append(num_excluded_samples)
            results['thickness_mean'].append(thickness_mean)
            results['height_mean'].append(height_mean)
            results['depth_mean'].append(depth_mean)
            results['depth_min'].append(depth_min)
            results['depth_max'].append(depth_max)
            results['range_mean'].append(range_mean)
            results['range_min'].append(range_min)
            results['range_max'].append(range_max)
            results['abc'].append(abc)
            results['nasc'].append(nasc)
            results['ping_start'].append(data_cell.axes[1][0]['ping'])
            results['ping_end'].append(data_cell.axes[1][-1]['ping'])
            results['num_valid_pings'].append(pd.np.sum(ping_thickness > 0))
            results['timestamp_start'].append(data_cell.axes[1][0]['reduced_timestamp'])
            results['timestamp_end'].append(data_cell.axes[1][-1]['reduced_timestamp'])
            results['mean_exclude_above'].append(mean_exclude_above)
            results['mean_exclude_below'].append(mean_exclude_below)
            results['lat_start'].append(lat_start)
            results['lat_end'].append(lat_end)
            results['lon_start'].append(lon_start)
            results['lon_end'].append(lon_end)


            #Individual mask results
            if min_threshold is not None:
                mask_results[('sample', 'exclude_below_threshold')].append(samples_below_min_threshold)

            if max_threshold is not None:
                mask_results[('sample', 'exclude_above_threshold')].append(samples_above_max_threshold)

            bad_pings = pd.np.zeros(data_cell.shape[1], dtype='bool')

            for mask_type, mask_group in masks.items():
                for mask_name, submask in mask_group.items():
                    masked_samples_per_ping = grid.get_data_by_cell(submask,
                                layer=layer_id,
                                interval=interval_id).sum(axis=0)

                    if mask_type == 'ping':
                        bad_pings |= masked_samples_per_ping > 0

                    masked_samples = masked_samples_per_ping.sum()
                    mask_results[(mask_type, mask_name)].append(masked_samples)

            results['num_filtered_pings'].append(bad_pings.sum())
            results['num_integrated_pings'].append(pd.np.sum(~bad_pings))

    if len(results_index_tuples) == 0:
        return None, None

    results_index = pd.MultiIndex.from_tuples(results_index_tuples, names=['interval', 'layer'])
    results_df = pd.DataFrame(results, index=results_index)
    results_df['timestamp_start'] = pd.to_datetime(results_df['timestamp_start'])
    results_df['timestamp_end'] = pd.to_datetime(results_df['timestamp_end'])

    if len(mask_results) > 0:
        mask_results_index = pd.MultiIndex.from_tuples(mask_results.keys(), names=['type', 'name'])
        mask_results_df = pd.DataFrame(mask_results, index=results_index, columns=mask_results_index)
    else:
        mask_results_df = None

    return results_df, mask_results_df