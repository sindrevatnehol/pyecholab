﻿# coding=utf-8

# Copyright (c) 2012, Zac Berkowitz
#     National Oceanic and Atmospheric Administration (NOAA)
#     Alaskan Fisheries Science Center (AFSC)
#     Resource Assessment and Conservation Engineering (RACE)
#     Midwater Assessment and Conservation Engineering (MACE)

# All rights reserved.

# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:

# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.

# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation 
#     and/or other materials provided with the distribution.

# 3.  Neither the names of NOAA, AFSC, RACE, or MACE nor the names of its 
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.


# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

u'''
.. :module:: echolab.plotting
    :synopsis: Plotting functions acoustic data

| Developed by:  Zac Berkowitz <zac.berkowitz@gmail.com> under contract for
| National Oceanic and Atmospheric Administration (NOAA)
| Alaska Fisheries Science Center (AFSC)
| Midwater Assesment and Conservation Engineering Group (MACE)
|
| Maintained by:
|       Zac Berkowitz <zac.berkowitz@gmail.com>
|       Rick Towler   <rick.towler@noaa.gov>
'''

import matplotlib as mpl
from matplotlib import pyplot as plt
import numpy as np

def _setup_ek500_cmap():
    '''
    Creates the ek500 colormap and boundary norm
    '''
    float_ek500_cmap_colorlist = [(0.62, 0.62, 0.62),
                            (0.37, 0.37, 0.37),
                            (0.0, 0.0, 1.0),
                            (0.0, 0.0, 0.498),
                            (0.0, 0.749, 0.0),
                            (0.0, 0.498, 0.0),
                            (1.0, 1.0, 0.0),
                            (1.0, 0.498, 0.0),
                            (1.0, 0.0, 0.749),
                            (1.0, 0.0, 0.0),
                            (0.651, 0.325, 0.235),
                            (0.471, 0.235, 0.157)]

    ek500_cmap = \
        mpl.colors.ListedColormap(float_ek500_cmap_colorlist, name='ek500')
    ek500_cmap.set_bad(color='k', alpha=1.0)
    ek500_cmap.set_under('w', alpha=1.0)
    ek500_cmap.set_over(color=float_ek500_cmap_colorlist[-1], alpha=1.0)
    
    if 'ek500' not in mpl.cm.cmap_d:
        mpl.cm.register_cmap('ek500', cmap=ek500_cmap)
    else:
        mpl.cm.cmap_d['ek500'] = ek500_cmap

_setup_ek500_cmap()
ek500_cmap = mpl.cm.get_cmap('ek500')
ek500_norm = mpl.colors.BoundaryNorm(range(-70, -33, 3), 12, clip=False)
ek500_full_integer_norm = mpl.colors.BoundaryNorm(range(-70, -33, 1), 36, clip=False)

def simple_echogram_plot(data, grid=None, ax=None, grid_window=20,
                         sample_unit='range'):
    '''
    Simple echogram plot using matplotlib
    
    :param data:  Data to plot 
    :type data:  :class:`echolab.AxesArray.AxesArray`
    
    :param grid:  Grid object
    :type grid: :class:`echolab.grid.Grid`
    
    :param ax:  Matplotlib axes object to draw plot on, None = new figure
    
    :param grid_window:  Window size for smoothing grid lines
    :type grid_window: int
    
    :param sample_unit:  Sample axes to use for plot -- 'sample' or 'range'
    :type sample_unit: str
    
    '''
    if ax is None:
        fig = plt.figure()
        ax = plt.subplot(111)
    else:
        fig = ax.get_figure()

    extents = [data.axes[1]['ping'][0],
               data.axes[1]['ping'][-1],
               data.axes[0][sample_unit][-1],
               data.axes[0][sample_unit][0]]
    
    img = ax.imshow(data,
              interpolation='nearest',
              cmap='ek500',
              norm=ek500_norm,
              extent=extents)

    reference = data.axes[1]['reference']

    if sample_unit == 'sample':
        reference = np.floor(reference / data.info['meters_per_sample'])

    reference = np.where(reference >= extents[-1], reference, np.nan) + \
        np.where(reference <= extents[-2], reference, np.nan)
        
    if not np.isnan(reference).all():
        ref = ax.plot(data.axes[1]['ping'], reference, color='k',
                      lw='3')
    else:
        ref = None

    plt.axis('tight')
    
    if sample_unit == 'sample':
        ax.set_ylabel('sample')
    
    elif sample_unit == 'range':
        ax.set_ylabel('range (m)')
    
    ax.set_xlabel('ping')
    plt.colorbar(img, ax=ax)

    if grid is not None:
        grid.plot(axes=ax, ma_window=grid_window)

    fig.canvas.draw()

    return fig


