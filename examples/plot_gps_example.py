# -*- coding: utf-8 -*-
"""
Created on Sat Aug 16 20:42:23 2014

@author: rick.towler

This simple example plots a cruise track using the NMEA data from
the .raw file. This can be done the easy way or the hard way. The easy way is
to call the interpolate_ping_dist method which will add 'lat' and 'lon' columns
to the raw_data.data DataFrame. This may reduce the number of fixes if your
GPS data is coming in faster than you were pinging but most often this is
acceptable. If you absolutely must have every GPS fix, you can call the get_nmea
method but then you will need to convert to decimal yourself.

THIS EXAMPLE REQUIRES THE BASEMAP MODULE.

http://pandas.pydata.org/pandas-docs/stable/basics.html

"""

import echolab
import echolab.plotting
import logging
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

#  set up logging - used to give feedback from the ecolab package
logging.basicConfig(level=logging.DEBUG)

#  define the paths to a data file
raw_filename = 'C:/Program Files (x86)/Simrad/Scientific/ER60/Examples/Survey/OfotenDemo-D20001214-T145902.raw'


#  read in the .raw data file - We just want GPS data so we set the ignore_datagram_types
#  keyword and pass it 'raw'. This returns a "RawReader" object that contains the data.
raw_data = echolab._io.raw_reader.RawReader([raw_filename], channel_numbers=[1],
                                            ignore_datagram_types=['raw'])

#  interpolate GPS data to ping-by-ping - conversion from DMS and westerly
#  longitude done automatically. We set the easterly_positive keyword to True
#  to return easterly coordinates as positive since that is what Basemap likes.
raw_data.interpolate_ping_gps(ignore_checksum=True, easterly_positive=True)

#  determine the min/max coordinates and place in list that will contain our
#  map boundaries. Since we're confident that all channels have matching ping
#  times we will only look at channel 1's lat/lon data
lat_bounds = [min(raw_data.data['lat'][1]),0,max(raw_data.data['lat'][1])]
lon_bounds = [min(raw_data.data['lon'][1]),0,max(raw_data.data['lon'][1])]

#  now calculate the rest of the expanded boundaries and mid-lines
r = (lat_bounds[2] - lat_bounds[0]) / 2.0
lat_bounds[1] = lat_bounds[0] + r
lat_bounds[0] = lat_bounds[0] - (r * 10)
lat_bounds[2] = lat_bounds[2] + (r * 10)

r = (lon_bounds[2] - lon_bounds[0]) / 2.0
lon_bounds[1] = lon_bounds[0] + r
lon_bounds[0] = lon_bounds[0] - (r * 2)
lon_bounds[2] = lon_bounds[2] + (r * 2)

#  now create a map projection so we can plot this on a map
map = Basemap(projection='lcc', lat_0 = lat_bounds[1], lon_0 = lon_bounds[1],
              resolution = 'h', area_thresh = 0.1, llcrnrlon=lon_bounds[0],
              llcrnrlat=lat_bounds[0],  urcrnrlon=lon_bounds[2],
              urcrnrlat=lat_bounds[2])

#  decorate the map
map.drawcoastlines()
map.drawmapboundary(fill_color='aqua')
map.fillcontinents(color='coral',lake_color='aqua')

#  draw the parallels and meridians
parallels = np.arange(round(lat_bounds[0],1),round(lat_bounds[2],1),0.1)
map.drawparallels(parallels,labels=[False,True,True,False])
meridians = np.arange(round(lon_bounds[0],1),round(lon_bounds[2],1),0.1)
map.drawmeridians(meridians,labels=[True,False,False,True])

#  convert our lat/lon values to x,y for plotting. Again we are only using the
#  GPS fixes from channel 1 since in the majority of cases these will be the
#  same for all channels.
x,y = map(raw_data.data['lon'][1].values, raw_data.data['lat'][1].values)

#  and then plot.
map.plot(x, y, linewidth=1.5,color='r')

#  show the map
plt.show(block=True)
