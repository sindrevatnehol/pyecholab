'''
Created on Mar 6, 2013

@author: zachary.berkowitz
'''

import numpy as np
from nose import tools as nt

import echolab

class test_SingleRangeReferenceSubmask(object):
    
    @classmethod
    def setupClass(cls):
        cls.shape = (5,5)
        info = {'meters_per_sample': 1}
        y_axes = np.empty((cls.shape[0],), dtype=np.dtype([('sample', np.int),
                                                    ('range', np.float)]))
        
        y_axes['sample'] = np.arange(cls.shape[0])
        y_axes['range'] = info['meters_per_sample'] * y_axes['sample']
        
        x_axes = np.empty(cls.shape[1], dtype=np.dtype([('ping', np.int), ('reference', np.float)]))
        x_axes['reference'][:] = 0
        x_axes['ping'][:] = np.arange(cls.shape[1])
        
        cls.data = echolab.AxesArray.AxesArray(np.zeros(cls.shape),
                        axes=[y_axes, x_axes], info=info)
        
    def test_mask_above_constant_reference_exclusive(self):
        
        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[4:, :] = True
        mask = echolab.mask.SingleRangeReferenceMask(3, '>')
        mask_result = mask.mask(self.data)
        
        nt.assert_true((mask_result==expected_result).all())
        
    def test_mask_above_constant_reference_inclusive(self):
        
        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[3:, :] = True
        mask = echolab.mask.SingleRangeReferenceMask(3, '>=')
        mask_result = mask.mask(self.data)
        
        nt.assert_true((mask_result==expected_result).all())        
    
    def test_mask_below_constant_reference_exclusive(self):
        
        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[:3, :] = True
        mask = echolab.mask.SingleRangeReferenceMask(3, '<')
        mask_result = mask.mask(self.data)
        
        nt.assert_true((mask_result==expected_result).all())
        
    def test_mask_below_constant_reference_inclusive(self):
        
        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[:4, :] = True
        mask = echolab.mask.SingleRangeReferenceMask(3, '<=')
        mask_result = mask.mask(self.data)
        
        nt.assert_true((mask_result==expected_result).all())
        
    def test_mask_reference_out_of_bounds(self):
        
        expected_result = np.zeros(self.shape, dtype='bool')
        mask = echolab.mask.SingleRangeReferenceMask(10, '>')
        mask_result = mask.mask(self.data)
        
        nt.assert_true((mask_result==expected_result).all())        
           
    def test_mask_reference_too_long(self):
        
        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[:3, :] = True
        
        reference = 3 * np.ones((self.shape[0] + 5,))
        mask = echolab.mask.SingleRangeReferenceMask(reference, '<')
        mask_result = mask.mask(self.data)
        
        nt.assert_true((mask_result==expected_result).all())
    
    @nt.raises(NotImplementedError)
    def test_reference_too_short(self):
        
        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[:3, :] = True
        
        reference = 3 * np.ones((self.shape[0] - 1,))
        mask = echolab.mask.SingleRangeReferenceMask(reference, '<')
        mask_result = mask.mask(self.data)
        
        nt.assert_true((mask_result==expected_result).all())        
        
    def test_variable_reference(self):
        
        reference = np.arange(self.shape[1])
        expected_result = np.zeros(self.shape, dtype='bool')
        for ping, ref in enumerate(reference):
            expected_result[ref:, ping] = True
            
        mask = echolab.mask.SingleRangeReferenceMask(reference, '>=')
        mask_result = mask.mask(self.data)
        
        nt.assert_true((mask_result==expected_result).all())
        
class test_DoubleRangeReferenceSubmask(object):
    
    @classmethod
    def setupClass(cls):
        cls.shape = (5,5)
        info = {'meters_per_sample': 1}
        y_axes = np.empty((cls.shape[0],), dtype=np.dtype([('sample', np.int),
                                                    ('range', np.float)]))
        
        y_axes['sample'] = np.arange(cls.shape[0])
        y_axes['range'] = info['meters_per_sample'] * y_axes['sample']
        
        x_axes = np.empty(cls.shape[1], dtype=np.dtype([('ping', np.int), ('reference', np.float)]))
        x_axes['reference'][:] = 0
        x_axes['ping'][:] = np.arange(cls.shape[1])
        
        cls.data = echolab.AxesArray.AxesArray(np.zeros(cls.shape),
                        axes=[y_axes, x_axes],
                        info=info)
        
    def test_mask_between_constant_reference_exclusive(self):
        

        mask = echolab.mask.DoubleRangeReferenceMask(2, 4, '><', exclusive=True)
        mask_result = mask.mask(self.data)

        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[3, :] = True
            
        nt.assert_true((mask_result==expected_result).all())

    def test_mask_between_constant_reference_inclusive(self):
        

        mask = echolab.mask.DoubleRangeReferenceMask(2, 4, '><', exclusive=False)
        mask_result = mask.mask(self.data)

        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[2:5, :] = True
            
        nt.assert_true((mask_result==expected_result).all())

    def test_mask_outside_constant_reference_exclusive(self):
        

        mask = echolab.mask.DoubleRangeReferenceMask(2, 4, '<>', exclusive=True)
        mask_result = mask.mask(self.data)

        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[0:2, :] = True
#        expected_result[4, :] = True
            
        nt.assert_true((mask_result==expected_result).all())

    def test_mask_outside_constant_reference_inclusive(self):
        

        mask = echolab.mask.DoubleRangeReferenceMask(2, 4, '<>', exclusive=False)
        mask_result = mask.mask(self.data)

        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[0:3, :] = True
        expected_result[4:, :] = True
        
            
        nt.assert_true((mask_result==expected_result).all())
    
    def test_mask_outside_upper_out_of_bounds(self):
        mask = echolab.mask.DoubleRangeReferenceMask(-1, 4, '<>', exclusive=False)
        mask_result = mask.mask(self.data)

        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[4:, :] = True
        
            
        nt.assert_true((mask_result==expected_result).all())
        
    def test_mask_outside_lower_out_of_bounds(self):
        mask = echolab.mask.DoubleRangeReferenceMask(2, 10, '<>', exclusive=False)
        mask_result = mask.mask(self.data)

        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[0:3, :] = True
        
        nt.assert_true((mask_result==expected_result).all())
        
    def test_mask_between_upper_out_of_bounds(self):
        mask = echolab.mask.DoubleRangeReferenceMask(-1, 4, '><', exclusive=False)
        mask_result = mask.mask(self.data)

        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[:5, :] = True
        
            
        nt.assert_true((mask_result==expected_result).all())
        
    def test_mask_between_lower_out_of_bounds(self):
        mask = echolab.mask.DoubleRangeReferenceMask(2, 10, '><', exclusive=False)
        mask_result = mask.mask(self.data)

        expected_result = np.zeros(self.shape, dtype='bool')
        expected_result[2:, :] = True
        
        nt.assert_true((mask_result==expected_result).all())
        

class test_PingFilter(object):
    
    @classmethod
    def setupClass(cls):
        cls.shape = (20,20)
        info = {'meters_per_sample': 1}
        y_axes = np.empty((cls.shape[0],), dtype=np.dtype([('sample', np.int),
                                                    ('range', np.float)]))
        
        y_axes['sample'] = np.arange(cls.shape[0])
        y_axes['range'] = info['meters_per_sample'] * y_axes['sample']
        
        x_axes = np.empty(cls.shape[1], dtype=np.dtype([('ping', np.int), ('reference', np.float)]))
        x_axes['reference'][:] = 0
        x_axes['ping'][:] = np.arange(cls.shape[1])
        
        cls.data = echolab.AxesArray.AxesArray(np.zeros(cls.shape),
                        axes=[y_axes, x_axes],
                        info=info)

        cls.good_value = 100        
        cls.data[:10, :] = cls.good_value
        cls.bad_pings_low = [3, 5, 10]
        cls.data[:, cls.bad_pings_low] = 0
        cls.bad_pings_high = [14, 17]
        cls.data[:, cls.bad_pings_high] = 120

    def test_no_deviation_exclude_above(self):
        
        mask = echolab.mask.PingFilter(upper_bound=0, column_length=5,
                 bound_unit='sample', threshold=self.good_value, 
                 deviation=None, mask_direction='>',
                 method='mean', deviation_type='absolute', exclusive=False)
        
        filter_results = mask.filter(self.data)
        mask_results   = mask.mask(self.data)
        
        expected_mask_results = np.zeros(self.data.shape, dtype='bool')
        expected_mask_results[:, self.bad_pings_high] = True
        
        nt.assert_true((filter_results == self.bad_pings_high).all())        
        nt.assert_true((mask_results == expected_mask_results).all())
        
    def test_no_deviation_exclude_below(self):
        
        mask = echolab.mask.PingFilter(upper_bound=0, column_length=5,
                 bound_unit='sample', threshold=self.good_value, 
                 deviation=None, mask_direction='<',
                 method='mean', deviation_type='absolute', exclusive=False)
        
        filter_results = mask.filter(self.data)
        mask_results   = mask.mask(self.data)
        
        expected_mask_results = np.zeros(self.data.shape, dtype='bool')
        expected_mask_results[:, self.bad_pings_low] = True
        
        nt.assert_true((filter_results == self.bad_pings_low).all())        
        nt.assert_true((mask_results == expected_mask_results).all())
        
    def test_no_deviation_exclude_any(self):
        
        mask = echolab.mask.PingFilter(upper_bound=0, column_length=5,
                 bound_unit='sample', threshold=self.good_value, 
                 deviation=None, mask_direction='!=',
                 method='mean', deviation_type='absolute', exclusive=False)
        
        filter_results = mask.filter(self.data)
        mask_results   = mask.mask(self.data)
        
        expected_bad_pings = sorted(self.bad_pings_high + self.bad_pings_low)
        expected_mask_results = np.zeros(self.data.shape, dtype='bool')
        expected_mask_results[:, expected_bad_pings] = True
        
        nt.assert_true((filter_results == expected_bad_pings).all())        
        nt.assert_true((mask_results == expected_mask_results).all())

    def test_perecent_deviation_exclude_outside(self):
        
        mask = echolab.mask.PingFilter(upper_bound=0, column_length=5,
                 bound_unit='sample', threshold=self.good_value, 
                 deviation=5, mask_direction='<>',
                 method='mean', deviation_type='percent', exclusive=False)
        
        filter_results = mask.filter(self.data)
        mask_results   = mask.mask(self.data)
        
        expected_bad_pings = sorted(self.bad_pings_high + self.bad_pings_low)
        expected_mask_results = np.zeros(self.data.shape, dtype='bool')
        expected_mask_results[:, expected_bad_pings] = True
        
        nt.assert_true((filter_results == expected_bad_pings).all())
        
    def test_absolute_deviation_exclude_outside(self):
        
        mask = echolab.mask.PingFilter(upper_bound=0, column_length=5,
                 bound_unit='sample', threshold=self.good_value, 
                 deviation=5, mask_direction='<>',
                 method='mean', deviation_type='absolute', exclusive=False)
        
        filter_results = mask.filter(self.data)
        mask_results   = mask.mask(self.data)
        
        expected_bad_pings = sorted(self.bad_pings_high + self.bad_pings_low)
        expected_mask_results = np.zeros(self.data.shape, dtype='bool')
        expected_mask_results[:, expected_bad_pings] = True
        
        nt.assert_true((filter_results == expected_bad_pings).all())                
        nt.assert_true((mask_results == expected_mask_results).all())

    def test_perecent_deviation_exclude_between(self):
        
        mask = echolab.mask.PingFilter(upper_bound=0, column_length=5,
                 bound_unit='sample', threshold=self.good_value, 
                 deviation=5, mask_direction='><',
                 method='mean', deviation_type='percent', exclusive=False)
        
        filter_results = mask.filter(self.data)
        mask_results   = mask.mask(self.data)
        
        expected_good_pings = sorted(self.bad_pings_high + self.bad_pings_low)
        expected_mask_results = np.zeros(self.data.shape, dtype='bool')
        expected_mask_results[:, expected_good_pings] = True
        expected_mask_results = ~expected_mask_results
        expected_bad_pings = np.arange(self.shape[1])[expected_mask_results[0,:]]
        
        nt.assert_true((filter_results == expected_bad_pings).all())
        nt.assert_true((mask_results == expected_mask_results).all())