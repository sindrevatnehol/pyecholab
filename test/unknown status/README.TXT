Files in this directory were used for testing during the development of
pyEcholab but their current working state is unknown. These need to be
reviewed and tests which are valid for the current iteration of pyEcholab
pulled out and put into service.

Tests for older versions of pyEcholab that no longer work should either
be modified to work or discarded.