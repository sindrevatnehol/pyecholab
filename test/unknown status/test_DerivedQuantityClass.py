import numpy as np
from echolab import _DerivedQuantity
from nose import tools as nt

import echolab.simrad_io
FILENAME = '..\\test\\data\\test_buddy_resize2-D20110712-T132550.raw'

er60_data = echolab.simrad_io.ER60Data(FILENAME,
        frequencies=[38e3], 
        nmea=['$SDVLW', '$GPGGA'],
        smooth_bottom_data=True)

class test_DerivedQuantity(object):
    
    
    def setUp(self):
        shape = (100, 100)
        self.DQ = _DerivedQuantity(shape, info=dict(one=1, two=2, three='three'))
                
    def test_equality(self):
        nt.assert_equal(self.DQ, self.DQ, "Equality self-check failed")
    
    def test_copy(self):
        other = self.DQ.copy()
        nt.assert_equals(self.DQ, other, "Copy equality failed")
        nt.assert_true(not (other is self.DQ), "Copy is a reference")
        
    def test_slicing(self):
        
        self.DQ[:, :] = np.arange(np.prod(self.DQ.shape)).reshape(self.DQ.shape)
        
        other = self.DQ[:5, :5]
        
        nt.assert_equals(other, self.DQ[:5, :5], "Slice does not match original portion")  
        
    def test_fromdata(self):

        
        power = er60_data.pings[0].power(gps_checksums=False)
        Sv = er60_data.pings[0].Sv(gps_checksums=False)
        sv = er60_data.pings[0].Sv(linear=True, gps_checksums=False)
        del Sv, sv

        Sp = er60_data.pings[0].Sp(gps_checksums=False)
        sp = er60_data.pings[0].Sp(linear=True, gps_checksums=False)
        del Sp, sp

        physical = er60_data.pings[0].physical_angle
        electrical = er60_data.pings[0].electrical_angle
        del physical, electrical

        alongship = er60_data.pings[0].alongship
        athwartship = er60_data.pings[0].athwartship
        del alongship, athwartship


        nt.assert_true(power.vlw is not None, "VLW not copied from data")
        nt.assert_true(power.gps is not None, "GPS not copied from data")
        nt.assert_true(power.transducer_depth is not None, "Transducer depths not copied from data")
        nt.assert_true(power.ping_times is not None, "Ping times not copied from data")
        

        
        other = power.copy()        
        nt.assert_equals(other, power, "Copying failed")


        sub = power[10:20, 30:40]
        nt.assert_equals(sub, power[10:20, 30:40], "Slicing failed")
           
    def test_append(self):

        orig_sv = er60_data.pings[0].Sv(gps_checksums=False)

        a = orig_sv[:, :500]
        b = orig_sv[:, 500:]

        c = a.append(b)

        nt.assert_equals(c, orig_sv, "Appending Failed")
    