import numpy as np
import echolab
from echolab.util import unit_conversion
from nose import tools as nt


FILENAME = '..\\test\\data\\test_buddy_resize2-D20110712-T132550.raw'

er60_data = echolab.ER60Data(FILENAME,
        frequencies=[38e3], 
        nmea=['$SDVLW', '$GPGGA'],
        smooth_bottom_data=True)

power = er60_data.pings[0].power(gps_checksums=False)._data

alongship_indexed = er60_data.pings[0].alongship(gps_checksums=False)._data
athwartship_indexed = er60_data.pings[0].athwartship(gps_checksums=False)._data

cal = er60_data.pings[0]._get_power_cal_params(0)
tvg_correction = 2

alongship_sensitivity = er60_data.pings[0].transceiver.angle_sensitivity_alongship
alongship_offset = er60_data.pings[0].transceiver.angle_offset_alongship

athwartship_sensitivity = er60_data.pings[0].transceiver.angle_sensitivity_athwartship
athwartship_offset = er60_data.pings[0].transceiver.angle_offset_athwartship



def test_Sv_conversions():   
    #log Sv
    valid = ~power.mask
    # import pdb; pdb.set_trace()
    Sv = unit_conversion.power_to_Sv(power, cal, tvg_correction,
        linear=False, inplace=False)
    
    power_ = unit_conversion.Sv_to_power(Sv, cal, tvg_correction,
                linear=False, inplace=False)

    nt.assert_true( (abs(power_[valid]- power[valid]) <= 0.1).all(), 'Log Sv conversion check failed')

    #lin Sv
    Sv = unit_conversion.power_to_Sv(power, cal, tvg_correction,
        linear=True, inplace=False)
    
    power_ = unit_conversion.Sv_to_power(Sv, cal, tvg_correction,
                linear=True, inplace=False)

    nt.assert_true( (abs(power_[valid]- power[valid]) <= 0.1).all(), 'Linear Sv conversion check failed')

def test_Sp_conversions():
    valid = ~power.mask
    #log Sp
    Sp = unit_conversion.power_to_Sp(power, cal, tvg_correction,
        linear=False, inplace=False)
    
    power_ = unit_conversion.Sp_to_power(Sp, cal, tvg_correction,
                linear=False, inplace=False)

    nt.assert_true( (abs(power_[valid]- power[valid]) <= 0.1).all(), 'Log Sp conversion check failed')

    #lin Sp
    Sp = unit_conversion.power_to_Sp(power, cal, tvg_correction,
        linear=True, inplace=False)
    
    power_ = unit_conversion.Sp_to_power(Sp, cal, tvg_correction,
                linear=True, inplace=False)

    nt.assert_true( (abs(power_[valid]- power[valid]) <= 0.1).all(), 'Linear Sp conversion check failed')

def test_Physical_angle_conversions():

    alongship_p = unit_conversion.indexed_to_physical_angle(alongship_indexed,
            alongship_sensitivity, alongship_offset)


    athwartship_p = unit_conversion.indexed_to_physical_angle(athwartship_indexed,
            athwartship_sensitivity, athwartship_offset)


    alongship_ = unit_conversion.physical_to_indexed_angle(alongship_p, alongship_sensitivity,
        alongship_offset)

    athwartship_ = unit_conversion.physical_to_indexed_angle(athwartship_p, athwartship_sensitivity,
        athwartship_offset)


    nt.assert_true((abs(alongship_ - alongship_indexed) <= 0.1).all(), 'Indexed -> Physical -> Indexed failed (alongship)')
    nt.assert_true((abs(athwartship_ - athwartship_indexed) <= 0.1).all(), 'Indexed -> Physical -> Indexed failed (athwartship)')


def test_Electrical_angle_conversions():

    alongship_e = unit_conversion.indexed_to_electrical_angle(alongship_indexed)


    athwartship_e = unit_conversion.indexed_to_electrical_angle(athwartship_indexed)


    alongship_ = unit_conversion.electrical_to_indexed_angle(alongship_e)

    athwartship_ = unit_conversion.electrical_to_indexed_angle(athwartship_e)


    nt.assert_true((abs(alongship_ - alongship_indexed) <= 0.1).all(), 'Indexed -> Electrical -> Indexed failed (alongship)')
    nt.assert_true((abs(athwartship_ - athwartship_indexed) <= 0.1).all(), 'Indexed -> Electrical -> Indexed failed (athwartship)')


def test_Electrical_Physical_angle_conversions():

    alongship_p = unit_conversion.indexed_to_physical_angle(alongship_indexed,
            alongship_sensitivity, alongship_offset)


    athwartship_p = unit_conversion.indexed_to_physical_angle(athwartship_indexed,
            athwartship_sensitivity, athwartship_offset)
            
            
    alongship_e = unit_conversion.indexed_to_electrical_angle(alongship_indexed)


    athwartship_e = unit_conversion.indexed_to_electrical_angle(athwartship_indexed)    



    alongship_p_ = unit_conversion.electrical_to_physical_angle(alongship_e,
        alongship_sensitivity, alongship_offset)

    athwartship_p_ = unit_conversion.electrical_to_physical_angle(athwartship_e,
        athwartship_sensitivity, athwartship_offset)

    alongship_e_ = unit_conversion.physical_to_electrical_angle(alongship_p,
        alongship_sensitivity, alongship_offset)

    athwartship_e_ = unit_conversion.physical_to_electrical_angle(athwartship_p,
        athwartship_sensitivity, athwartship_offset)

    nt.assert_true((abs(alongship_p - alongship_p_) <= 0.1).all(), 'Physical -> Electrical -> Physical failed (alongship)')
    nt.assert_true((abs(athwartship_p - athwartship_p_) <= 0.1).all(), 'Physical -> Electrical -> Physical failed (athwartship)')

    nt.assert_true((abs(alongship_e - alongship_e_) <= 0.1).all(), 'Electrical -> Physical -> Electrical failed (alongship)')
    nt.assert_true((abs(athwartship_e - athwartship_e_) <= 0.1).all(), 'Electrical -> Physical -> Electrical failed (athwartship)')


