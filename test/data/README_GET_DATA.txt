Test data package can be downloaded from:

https://www.dropbox.com/s/jqn4ialouqfasir/pyEcholabTestData.zip?dl=0

Unzip this file into your pyEcholab distribution directory. It should then
add the data files to the ./test/data directory.